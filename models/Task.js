const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema({
    user : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required : true,
        immutable: true
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum : ['new', 'in_progress', 'complete'],
        default: 'new'
    }
})

const Task = mongoose.model('Task', TaskSchema);


module.exports = Task;