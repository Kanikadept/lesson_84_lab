const express = require('express');

const Task = require('../models/Task');
const auth = require("../myMiddleWare/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {

    try {
        const task = new Task(req.body);
        task.user = req.user;
        await task.save();
        res.send(task);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const tasks = await Task.find({user: req.user});
        res.send(tasks);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.put('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndUpdate({user: req.user, _id: req.params.id},
                                                        req.body,
                                                {new: true});
        res.send(task);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({user: req.user, _id: req.params.id});
        res.send(task);
    } catch (error) {
        res.status(400).send(error);
    }
});

module.exports = router;